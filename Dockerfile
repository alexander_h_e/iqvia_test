FROM python:3.6.12-alpine3.12

WORKDIR /app
ADD app/ /app
RUN cd /app && pip3 install -r requirements.txt

EXPOSE 8000
CMD ["sh","-c","gunicorn server:app -b 0.0.0.0 && celery -A worker worker --loglevel=debug"]
