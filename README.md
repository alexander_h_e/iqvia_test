### Test app

Everything is dockerized, so all that needs to be done is:

```
$ docker-compose build --no-cache
```

Followed by

```
$ docker-compose up
```

Used mysql given that I alredy had an image pulled on my computer. All the database schemas will be loaded into the mysql container automatically and the application will start on port 8000 in a few seconds.

Endpoints with example usage can be found in app/server.py
