import datetime
from sqlalchemy.ext.declarative import declarative_base
import logging
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from database.config import CONFIG_


LOGGER = logging.getLogger("iqvia_test")


def _get_session() -> sessionmaker:
    """Creates an sqlalchemy session from the configuration.
    Returns
    -------
    sessionmaker"""
    enigne = create_engine(
        CONFIG_.get("dbconn"),
        echo=bool(int(CONFIG_.get("dblogging")))
    )
    session = sessionmaker(bind=enigne)()
    logging.getLogger('sqlalchemy').addHandler(LOGGER)
    return session


class DictExtractor:
    """Extend this model in order to be able
    to return a dict from a result."""

    def as_dict(self):
        """Given that much of the application will return
        JSON strings, this simply transforms a result into
        a JSON-serializable dict.
        Returns
        -------
        dict"""
        return {c.name: DictExtractor._transform(getattr(self, c.name))
                for c in self.__table__.columns}

    @staticmethod
    def _transform(variable):
        """Transforms a variable to a json-serializable data-type."""
        if isinstance(variable, datetime.datetime):
            return variable.isoformat()
        return variable


class DB:
    """Wrapper around sqlalchemy's session, largely to enable
    a `with` block and would potentially allow greater flexibility
    if needed for more specific use cases, logging or configurations."""
    __slots__ = [
        "_session"
    ]

    def __init__(self):
        self._session = _get_session()

    @property
    def session(self):
        """Getter for the private property holding the session."""
        return self._session

    def __enter__(self):
        """Creates a session, nothing more
        and returns it so that anything can be added within the `with`
        block."""
        self._session = _get_session()
        return self.session

    def __exit__(self, *args, **kwargs):
        """As opposed to using autocommit, this takes care of committing
        at the end of the `with` block."""
        self._session.commit()


Base = declarative_base()
