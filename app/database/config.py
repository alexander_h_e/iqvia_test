import os
from configparser import ConfigParser


class Config:
    """Using this instead of flask's built-in config
    in case something like vault needs to be put in place."""
    __slots__ = [
        "_config",
        "_config_file"
    ]

    def __init__(self, config_file: str = None):
        if config_file:
            self._config_file = config_file
        else:
            self._config_file = os.environ.get(
                "iqviatest_configfile"
            )
        self._parse_config()

    def _parse_config(self):
        self._config = ConfigParser()
        self._config.read(self._config_file)

    @property
    def config(self):
        return self._config

    def get(self, key, section="APPCONFIG", default=None):
        return self.config[section].get(key, default)


CONFIG_ = Config()
