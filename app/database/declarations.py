import datetime
from sqlalchemy import Column, Integer, String, DateTime, SmallInteger, ForeignKey
from sqlalchemy.orm import relationship
from database.base_ import Base, DictExtractor


class Contacts(Base, DictExtractor):
    """Declaration fot the contacts table."""
    __tablename__ = "contacts"

    contact_id = Column("contact_id", Integer, primary_key=True)
    username = Column("username", String(42), nullable=False, unique=True)
    confirmed = Column("confirmed", SmallInteger, nullable=False, default=0)
    name = Column("name", String(42), nullable=False)
    surname = Column("surname", String(42), nullable=False)
    created = Column("created", DateTime, nullable=False, default=datetime.datetime.utcnow)

    def __repr__(self):
        return f"<{self.__tablename__}(\n" \
               f"\tcontact_id = {self.contact_id}\n" \
               f"\tusername = {self.username}\n" \
               f"\tconfirmed = {self.confirmed}\n" \
               f"\tname = {self.name}\n" \
               f"\tsurname = {self.surname}\n" \
               f"\tcreated = {self.created}\n)>"


#
class Emails(Base, DictExtractor):
    """Declaration for the emails table."""
    __tablename__ = "emails"

    email_id = Column("email_id", Integer, primary_key=True)
    f_contact_id = Column(
        Integer,
        ForeignKey(f"{Contacts.__tablename__}.contact_id")
    )
    email = Column("email", Integer, nullable=False)
    is_primary = Column("is_primary", Integer, nullable=False, default=0)
    bs = relationship("Contacts")

    def __repr__(self):
        return f"<{self.__tablename__}(\n" \
               f"\temail_id = {self.email_id}\n" \
               f"\temail = {self.email}\n" \
               f"\tf_contact_id = {self.f_contact_id}\n" \
               f"\tis_primary = {self.is_primary}\n)>"
