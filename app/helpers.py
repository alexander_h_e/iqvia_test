import logging
from database.declarations import Contacts, Emails
from database.base_ import DB
from database.config import CONFIG_


LOGGER = logging.getLogger("iqvia_test")


def add_user(userdata, email):
    try:
        with DB() as session:
            contact = Contacts(
                **userdata
            )
            session.add(contact)
            session.flush()
            email = Emails(
                f_contact_id=contact.contact_id,
                email=email,
                is_primary=1
            )

            session.add(email)
    except Exception as e:
        LOGGER.error("Unable to add user: {e}")
        return False
    return True


