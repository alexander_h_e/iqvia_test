from logging.handlers import RotatingFileHandler
import logging
from database.config import CONFIG_


LOGGER = logging.getLogger("iqvia_test")
LOGGER.setLevel(getattr(logging, CONFIG_.get("loglevel").upper()))
handler = RotatingFileHandler(CONFIG_.get("logfile"), maxBytes=15360, backupCount=6)
handler.setFormatter(logging.Formatter("[%(asctime)s : %(levelname)s]: %(message)s"))