import datetime
import threading
from collections import ChainMap
from flask import Flask, jsonify, request, Response
from database.declarations import Contacts, Emails
from helpers import add_user
from database.base_ import DB
from database.config import CONFIG_
from worker import add_new_user, remove_old_users
from logger import LOGGER


"""Considering both creating new users
and removing old ones is an inexpensive
operation, starting an infinite loop
from the main application in celery
with threading is sufficient."""


def schedule_new():
    """Starts a celery task which creates a new
    user every 15 seconds(or whatever has been set
    inside the configuration file.

    Returns
    -------
    None"""
    add_new_user().delay()

def schedule_gc():
    """Starts a celery task which deletes all
    users created over 1 minute ago(or whatever has
    been set inside the configuration file.

    Returns
    -------
    None"""
    remove_old_users().delay()


if bool(CONFIG_.get("create_dummy_data")):
    threading.Thread(target=schedule_new, args=(), daemon=True).start()
    threading.Thread(target=schedule_gc, args=(), daemon=True).start()


app = Flask(__name__)


@app.route('/')
def index():
    return '''
    <pre>Hi!
           .-/`)
          / / / )
         / / / / )
      /`/ / / / /
     / /     ` /
    |         /
     \       /
      )    .'
     /    /
         /</pre>
    '''


@app.route('/list_contacts')
def list_contacts():
    """Lists all the contacts from the database.
    They may appear multiple times if the contact
    has multiple emails registered and the query
    uses an inner join from email to contacts.

    Returns
    -------
    flask.jsonify"""
    with DB() as session:
        contacts = [
            map(lambda x: x.as_dict(), i)
            for i in session.query(Emails, Contacts).join(
                Contacts,
                Contacts.contact_id == Emails.f_contact_id
            ).all()
        ]

        # If the data becomes too big, offset should be added as logic.
        if len(contacts) > int(CONFIG_.get("max_results_warning")):
            LOGGER.warning(
                f"There are more than {CONFIG_.get('max_results_warning')} "
                f"in the result, this could cause potential issues."
            )

        return jsonify([dict(ChainMap(*c)) for c in contacts])


@app.route('/search', methods=["GET"])
def search():
    """Searches for a user by username.
    Ex.:
        $ curl {{.endpoint}}/search?name=example

    Returns
    -------
    flask.jsonify"""
    if request.args.get("name", ""):
        with DB() as session:
            contacts = [
                map(lambda x: x.as_dict(), i)
                for i in session.query(Emails, Contacts).join(
                    Contacts,
                    Contacts.contact_id == Emails.f_contact_id
                ).filter_by(
                    username=request.args.get("name", "")
                ).all()
            ]
        return jsonify([dict(ChainMap(*c)) for c in contacts])
    return jsonify([])


@app.route('/create', methods=["POST"])
def create():
    """Creates a new contact from input parameters
    ex. request:
        $ curl -d "username=test1&name=test_name&surname=test2&email=a@b.c" -X POST {{.endpoint}}/create

    Returns
    -------
    flask.Response"""
    data = {
        param: request.form.get(param)
        for param in ["username", "name", "surname"]
    }
    email = request.form.get('email')

    if not all(data.values()):

        return Response(
            f"All parameters : {', '.join(list(data.keys()))} are "
            f"mandatory!", status=400)
    if add_user(data, email):
        return Response("User added successfully.", 200)
    return Response("User could not be added.", status=500)


@app.route('/delete', methods=["DELETE"])
def delete():
    """Deletes an existing user and their data.
    Ex.:
        $ curl -d "username=test1&name=test_name&surname=test2&email=a@b.c" -X DELETE {{.endpoint}}/delete


    Returns
    -------
    str"""
    user = request.form.get("username")
    if not user:
        return "username parameter must be supplied"
    with DB() as session:
        user = session.query(Emails, Contacts).join(
            Contacts,
            Contacts.contact_id == Emails.f_contact_id
        ).filter_by(
            username=user
        ).all()
        # For easier debugging...
        for x in user:
            LOGGER.debug(x)
            session.delete(x[0])
            session.delete(x[1])
    return request.form.get("username")


@app.route('/add_email', methods=["POST"])
def add_email():
    """Adds a new email to an existing user"""
    user = request.form.get("username")
    with DB() as session:
        try:
            user = session.query(
                Contacts
            ).filter_by(username=user).one()
            email = Emails(
                f_contact_id=user.contact_id,
                email=request.form.get("email"),
                is_primary=0
            )
            session.add(email)
            message, status = "Email added.", 200
        except Exception as e:
            LOGGER.error("Email not added: {}".format(str(e)))
            message, status = "Unable to add email.", 500
        finally:
            return Response(message, status=status)


@app.route("/edit_mail", methods=["POST"])
def edit_mail():
    """Sets an email as primary. The ida behind
    primary emails is that a contact should have
    at least one email as a bare minimum.
    ex:
        $ curl -d "email=a@b.c&username=test_user" -X POST {{.endpoint}}/edit_mail


    Returns
    -------
    str"""
    user, email = request.form.get("username"), request.form.get("email")
    with DB() as session:
        try:
            email = session.query(Emails).join(
                Contacts,
                Contacts.contact_id == Emails.f_contact_id
            ).filter(
                Contacts.username == user,
                Emails.email == email
            ).one()
            email.is_primary = True
            id_ = email.email_id
            emails = session.query(Emails).join(
                Contacts,
                Contacts.contact_id == Emails.f_contact_id
            ).filter(
                Contacts.username == user,
                Emails.email_id != id_
            ).all()
            for email in emails:
                email.is_primary = False
            message = "OK"
        except Exception as e:
            LOGGER.error("Email not set as primary: {}".format(str(e)))
            message = "Unable to set email as primary"
        finally:
            return message


@app.route('/delete_mail', methods=["DELETE"])
def delete_mail():
    """Deletes a mail for a given contact, granted it's not
    the primary email. If it is, a new email must be specified
    as primary first.

    Returns
    -------
    flask.Response"""
    user, email = request.form.get("username"), request.form.get("email")
    with DB() as session:
        try:
            email = session.query(Emails).join(
                Contacts,
                Contacts.contact_id == Emails.f_contact_id
            ).filter(
                Contacts.username == user,
                Emails.email == email
            ).one()
            if not email.is_primary:
                session.delete(email)
                message, status = "Email deleted.", 200
            else:
                message, status = "Cannot delete primary email.", 400
        except Exception as e:
            LOGGER.debug("Unable to delete email: {}".format(e))
            message, status = "Unable to delete email", 500
        finally:
            return Response(message, status=status)


@app.route('/update', methods=["POST"])
def update():
    """A method to update a user's name, surname
    or surname(whichever is specified).
    Ex.:
        $ curl -d 'name=new_name&surname=new_surname&username=test' -X POST {{.endpoint}}/update"""
    params_dict = {i: request.form.get(i) for i in
        ["name", "surname"]}
    username = request.form.get("username")
    with DB() as session:
        try:
            contact = session.query(
                Contacts
            ).filter(Contacts.username == username).one()
            for k, v in params_dict.items():
                if v:
                    setattr(contact, k, v)
            message = "Data updated successfully."
        except Exception as e:
            LOGGER.debug(e)
            message = "Unable to update contact"
        finally:
            return message

@app.route('/delete_oldest')
def delete_oldest():
    """Deletes all contacts that were created over 60 seconds ago."""
    one_min = datetime.datetime.utcnow() - datetime.timedelta(seconds=int(CONFIG_.get("delete_older_than")))
    with DB() as session:
        for old in session.query(Contacts).filter(Contacts.created < one_min).all():
            LOGGER.debug("Deleting: {}".format(str(old)))
            session.delete(old)
    return "[]"
