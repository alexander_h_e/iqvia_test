import logging
import time
import uuid
import hashlib
import requests
from celery import Celery
from database.config import CONFIG_

LOGGER = logging.getLogger("iqvia_test")
broker = f"redis://{CONFIG_.get('redis_host')}" \
         f":{CONFIG_.get('redis_port')}/" \
         f"{CONFIG_.get('worker')}"
LOGGER.debug(broker)

app = Celery('tasks', broker=broker)

@app.task
def add_new_user():
    """Calls the /create API every X seconds"""
    while True:
        email = hashlib.md5(str(uuid.uuid4()).encode()).hexdigest()
        data = {
            "username": hashlib.md5(str(uuid.uuid4()).encode()).hexdigest(),
            "name": hashlib.md5(str(uuid.uuid4()).encode()).hexdigest(),
            "surname": hashlib.md5(str(uuid.uuid4()).encode()).hexdigest(),
            "email": f"{email}@{email}.com"
        }
        resp = requests.post(f"{CONFIG_.get('endpoint')}/create", data=data)
        LOGGER.info(f"{str(data)} : {resp.text}")
        time.sleep(int(CONFIG_.get("create_new")))

@app.task
def remove_old_users():
    """Calls the /delete_oldest API every X seconds"""
    while True:
        resp = requests.get(f"{CONFIG_.get('endpoint')}/delete_oldest")
        LOGGER.info(resp)
        time.sleep(int(CONFIG_.get("delete_interval")))
