CREATE DATABASE `iqvia_test`;
USE `iqvia_test`;

CREATE TABLE `contacts` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(42) NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(42) NOT NULL,
  `surname` varchar(42) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`contact_id`),
  KEY `username` (`username`),
  KEY `confirmed` (`confirmed`),
  KEY `created` (`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `emails` (
  `email_id` int(11) NOT NULL AUTO_INCREMENT,
  `f_contact_id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `is_primary` tinyint(1) NOT NULL,
  PRIMARY KEY (`email_id`),
  KEY `f_contact_id` (`f_contact_id`),
  KEY `email` (`email`),
  KEY `is_primary` (`is_primary`),
  CONSTRAINT `emails_ibfk_1` FOREIGN KEY (`f_contact_id`) REFERENCES `contacts` (`contact_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

GRANT ALL PRIVILEGES ON *.* TO 'toor'@'%';
SET GLOBAL max_allowed_packet = 268435456;