import unittest
import uuid
import hashlib
import requests

class testLanguageMethods(unittest.TestCase):

    def test_create_new_user(self):
        email = hashlib.md5(str(uuid.uuid4()).encode()).hexdigest()
        data = {
            "username": hashlib.md5(str(uuid.uuid4()).encode()).hexdigest(),
            "name": hashlib.md5(str(uuid.uuid4()).encode()).hexdigest(),
            "surname": hashlib.md5(str(uuid.uuid4()).encode()).hexdigest(),
            "email": f"{email}@{email}.com"
        }
        resp = requests.post(f"http://localhost:8000/create", data=data)
        self.assertEqual(resp.status_code, 200)

    def test_create_existing_user(self):
        email = hashlib.md5(str(uuid.uuid4()).encode()).hexdigest()
        data = {
            "username": hashlib.md5(str(uuid.uuid4()).encode()).hexdigest(),
            "name": hashlib.md5(str(uuid.uuid4()).encode()).hexdigest(),
            "surname": hashlib.md5(str(uuid.uuid4()).encode()).hexdigest(),
            "email": f"{email}@{email}.com"
        }
        requests.post(f"http://localhost:8000/create", data=data)
        resp = requests.post(f"http://localhost:8000/create", data=data)
        self.assertNotEqual(resp, 200)

    def test_list_all_users(self):
        resp = ""
        try:
            resp = requests.get("http://localhost:8000/list_contacts").json()
        except:
            pass
        self.assertIsInstance(resp, list)

    def test_add_email(self):
        email = hashlib.md5(str(uuid.uuid4()).encode()).hexdigest()
        data = {
            "username": hashlib.md5(str(uuid.uuid4()).encode()).hexdigest(),
            "name": hashlib.md5(str(uuid.uuid4()).encode()).hexdigest(),
            "surname": hashlib.md5(str(uuid.uuid4()).encode()).hexdigest(),
            "email": f"{email}@{email}.com"
        }
        resp = requests.post(f"http://localhost:8000/create", data=data)
        email = hashlib.md5(str(uuid.uuid4()).encode()).hexdigest()
        resp = requests.post(f"http://localhost:8000/add_email", data={
            "username": data['username'],
            "email": f"{email}@{email}.com"
        })
        self.assertEqual(resp.status_code, 200)

    def test_add_email_fail(self):
        email = hashlib.md5(str(uuid.uuid4()).encode()).hexdigest()
        data = {
            "username": hashlib.md5(str(uuid.uuid4()).encode()).hexdigest(),
            "name": hashlib.md5(str(uuid.uuid4()).encode()).hexdigest(),
            "surname": hashlib.md5(str(uuid.uuid4()).encode()).hexdigest(),
            "email": f"{email}@{email}.com"
        }
        resp = requests.post(f"http://localhost:8000/create", data=data)
        email = hashlib.md5(str(uuid.uuid4()).encode()).hexdigest()
        resp = requests.post(f"http://localhost:8000/add_email", data={
            "username": data['username'],
            "email": f"{email}@{email}.com"
        })
        resp = requests.post(f"http://localhost:8000/add_email", data={
            "username": data['username'],
            "email": f"{email}@{email}.com"
        })
        self.assertNotEqual(resp, 200)
